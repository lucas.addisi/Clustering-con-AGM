package capaGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import capaAlmacenamiento.Archivo;
import capaGrafica.Marcadores;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


public class VentanaExplorador extends JFrame {
	
	private static final long serialVersionUID = -5533610893665482746L;

	private JPanel contentPane;
	private JFileChooser fileChooser = new JFileChooser();
	private Marcadores marcadores;
	public Archivo archivo = new Archivo();

	public VentanaExplorador(Marcadores marc) {

		marcadores = marc;
		
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);

		contentPane = new JPanel();

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		explorar();
		botonCargar();
		botonRegresar();

	}
	
	private void botonRegresar() {
		JButton regresar = new JButton("Regresar");
		regresar.setBounds(600, 100, 117, 25);
		contentPane.add(regresar);
		
		regresar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});

	}
	
	private void botonCargar() {
		JButton cargar = new JButton("Cargar");
		cargar.setBounds(600, 350, 117, 25);
		contentPane.add(cargar);
		
		cargar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
		
				File file = fileChooser.getSelectedFile();
				String fileName = file.getAbsolutePath();
				
				try {
					for (Archivo.Coordenada coor : archivo.cargarGrafo(fileName))
						marcadores.agregarMarcador(coor.getLat(), coor.getLon());					
				} catch (Exception ex) {
					marcadores.eliminarMarcadores();
					JOptionPane.showMessageDialog(null, ex.toString());
				}		
			}
		});
	}
	
	private void explorar() {
		fileChooser.setBounds(35, 21, 507, 328);
		contentPane.add(fileChooser);		
		fileChooser.setVisible(true);

	}
	
}
