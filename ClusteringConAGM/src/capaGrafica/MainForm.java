package capaGrafica;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JButton;
import javax.swing.SwingConstants;

public class MainForm {

	private VentanaMapa mapa;
	private VentanaMarcador ventanaMarcador;
	private VentanaExplorador explorador;
	
	private JFrame frame;
	private JLabel lblNewLabel;
	
	private Marcadores marc;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() {
		initialize();
	}

	private void initialize() {
		marc = new Marcadores();
		
		frame = new JFrame();
		frame.setResizable(false);
	
		mapa = new VentanaMapa(marc);
		ventanaMarcador = new VentanaMarcador(marc);
		explorador = new VentanaExplorador(marc);

		frame.setBounds(100, 100, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		jLabelCabecera();
		botonMostrarMapa();
		botonVentanaMarcador();
		botonExplorar();
	}

	private void jLabelCabecera() {
		lblNewLabel = new JLabel("Clusters");
		lblNewLabel.setFont(new Font("Yu Mincho Demibold", Font.BOLD | Font.ITALIC, 20));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(341, 45, 149, 50);
		frame.getContentPane().add(lblNewLabel);

	}

	private void botonMostrarMapa() {
		JButton button = new JButton("Mapa");
		button.setBounds(341, 164, 150, 50);
		frame.getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapa.actualizarMarcadores();
				ventanaMarcador.setVisible(false);
				mapa.setVisible(true);
			}

		});
	}

	private void botonVentanaMarcador() {
		JButton botonColocarMarcador = new JButton("Colocar Marcador");
		botonColocarMarcador.setBounds(341, 224, 150, 50);
		frame.getContentPane().add(botonColocarMarcador);
		botonColocarMarcador.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ventanaMarcador.setVisible(true);
				mapa.setVisible(false);
			}

		});
	}

	private void botonExplorar() {
		JButton	botonExplorar = new JButton("Explorar");
		botonExplorar.setBounds(341, 284, 150, 50);
		frame.getContentPane().add(botonExplorar);

			
		botonExplorar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ventanaMarcador.setVisible(false);
				mapa.setVisible(false);
				explorador.setVisible(true);
			}

		});	
	}	
	
}
