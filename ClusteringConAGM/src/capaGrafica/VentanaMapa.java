package capaGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import capaAlmacenamiento.Archivo;

import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JTextField;

public class VentanaMapa extends JFrame {
	private static final long serialVersionUID = 3450275075573527232L;
	private JMapViewer miMapa;
	private JButton botonCluster;
	private JToggleButton insertarNodo;
	boolean selected = false;

	private Archivo archivo = new Archivo();
	private Marcadores marcadores;
	ArrayList<Marcadores> clusters;
	private ArrayList<MapPolygon> clustersEnPoligonos;
	private JTextField textField;

	public VentanaMapa(Marcadores marc) {
		marcadores = marc;
		clusters = new ArrayList<>();
		clustersEnPoligonos = new ArrayList<MapPolygon>();

		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);

		inicializadorMapa();
		fieldIngresarNumDeClusters();
		botonGuardar();
		botonGenerarCluster();
		botonInsertarNodo();
		botonEliminar();
		botonRegresar();
	}

	private void inicializadorMapa() {
		miMapa = new JMapViewer();

		miMapa.setZoomContolsVisible(false);
		miMapa.setDisplayPositionByLatLon(-34.521, -58.7008, 14);

		setContentPane(miMapa);
	}

	private void botonRegresar() {
		JButton regresar = new JButton("Regresar");
		regresar.setBounds(0, 0, 117, 25);
		miMapa.setLayout(null);
		miMapa.add(botonCluster);
		miMapa.add(insertarNodo);
		miMapa.add(regresar);

		regresar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}

		});
	}

	private void botonEliminar() {
		JButton btnNewButton_1 = new JButton("Eliminar nodos");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				eliminarMarcadores();
				eliminarPoligonos();
				actualizarMarcadores();
			}
		});
		btnNewButton_1.setBounds(10, 448, 149, 23);
		miMapa.add(btnNewButton_1);
	}

	private void botonGenerarCluster() {
		botonCluster = new JButton("GenerarClusters");
		botonCluster.setBounds(416, 448, 149, 23);
		botonCluster.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {
					eliminarPoligonos();
					clusters.clear();
					clusters = marcadores.generarCluster(obetenerNumeroDeJTextField());
					for (Marcadores mar : clusters) {
						ArrayList<MapPolygon> poligono = mar.graficarCluster();
						clustersEnPoligonos.addAll(poligono);
					}
				} catch (Exception e) {
					e.getMessage();
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
				actualizarMarcadores();
			}
		});
	}

	private void botonInsertarNodo() {
		insertarNodo = new JToggleButton("Insertar nodo");
		insertarNodo.setBounds(635, 448, 149, 23);
		insertarNodo.addActionListener(new ActionListener() {

			MouseListener mouse = new MouseListener() {

				@Override
				public void mouseClicked(MouseEvent e) {
					try {

						Coordinate a = miMapa.getPosition(miMapa.getMousePosition());
						marcadores.agregarMarcador(a.getLat(), a.getLon());
					} catch (Exception a) {
					}

					actualizarMarcadores();
				}

				@Override
				public void mouseEntered(MouseEvent e) {
				}

				@Override
				public void mouseExited(MouseEvent e) {
				}

				@Override
				public void mousePressed(MouseEvent e) {
				}

				@Override
				public void mouseReleased(MouseEvent e) {
				}

			};

			@Override
			public void actionPerformed(ActionEvent e) {
				insertarNodo.setSelected(!selected);
				selected = !selected;

				if (insertarNodo.isSelected()) {
					miMapa.addMouseListener(mouse);
				} else
					miMapa.removeMouseListener(mouse);
			}

		});

	}

	private void botonGuardar() {
		JButton guardar = new JButton("Guardar");
		guardar.setBounds(665, 0, 117, 25);
		miMapa.add(guardar);

		guardar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				try {
					archivo.guardarGrafo(marcadores);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
				}
			}
		});

	}

	private void fieldIngresarNumDeClusters() {
		textField = new JTextField();
		textField.setBounds(575, 449, 37, 20);
		miMapa.add(textField);
		textField.setColumns(10);
	}

	public void actualizarMarcadores() {

		for (MapMarker mar : marcadores.getMarcadores())
			miMapa.addMapMarker(mar);

		for (MapPolygon clus : clustersEnPoligonos)
			miMapa.addMapPolygon(clus);
	}

	private void eliminarMarcadores() {
		miMapa.removeAllMapMarkers();
		marcadores.eliminarMarcadores();
	}

	private void eliminarPoligonos() {
		miMapa.removeAllMapPolygons();
		clustersEnPoligonos.clear();
	}

	private Integer obetenerNumeroDeJTextField() {
		try {
			Integer ret = Integer.parseInt(textField.getText());
			return ret;
		} catch (Exception e) {
			throw new IllegalArgumentException("Argumento invalido, ingrese un numero v�lido");
		}

	}

}
