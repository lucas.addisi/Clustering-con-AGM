package capaGrafica;
import java.util.ArrayList;
import java.util.Iterator;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import capaLogica.Conversor;

public class Marcadores implements Iterable<MapMarker> {
	private ArrayList<MapMarker> marcadores;
	
	public Marcadores(){
		marcadores = new ArrayList<MapMarker>();
	}
	
	public int cantidadDeMarcadores() {
		return marcadores.size();
	}
	
	public ArrayList<MapMarker> getMarcadores(){
		return marcadores;
	}
	
	public void eliminarMarcadores() {
		marcadores.removeAll(getMarcadores());
	}
	
	public void agregarMarcador(double lat, double lon){
		MapMarker marcador = new MapMarkerDot(lat, lon);
		if (! estaContenido(marcador))
			marcadores.add(marcador);
		else
			throw new IllegalArgumentException("Marcador ya ingresado: Latitud = " + lat + " Longitud = " + lon);
	}
	
	public ArrayList<Marcadores> generarCluster(int numCluster){
		ArrayList<Marcadores> ret = new ArrayList<>();
		Conversor convertir = new Conversor(this);
		ret = convertir.clustersEnMarcadores(numCluster);
		return ret;
	}
	
	public ArrayList<MapPolygon> graficarCluster(){
		ArrayList<MapPolygon> ret = new ArrayList<>();
		
		for (int i = 0, k = 1  ; i < cantidadDeMarcadores() -1; ++i, k++){
			ArrayList<Coordinate> coordenadas = new ArrayList<>();
			coordenadas.add(new Coordinate(marcadores.get(i).getLat() , marcadores.get(i).getLon()));
			coordenadas.add(new Coordinate(marcadores.get(k).getLat() , marcadores.get(k).getLon()));
			coordenadas.add(new Coordinate(marcadores.get(i).getLat() , marcadores.get(i).getLon()));
			
			ret.add(new MapPolygonImpl(coordenadas));
		}
		
		return ret;
	}
	
	@Override
	public Iterator<MapMarker> iterator() {
		return marcadores.iterator();
	}
	
	public boolean equals(Object obj){
		boolean ret = true;
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		
		ret &= marcadores.size() == ((Marcadores)obj).marcadores.size();
		int i = 0;
		for(MapMarker m : marcadores){
			ret &= m.getLat() == ((Marcadores)obj).marcadores.get(i).getLat()
					&& m.getLon()== ((Marcadores)obj).marcadores.get(i).getLon();
			i++;
		}
		return ret;
	}
	
	public String toString(){
		String ret = "[";
		for(MapMarker c : marcadores)
		{
			ret += "Latitud: " + c.getLat() + 
					" Longitud: " + c.getLon() + "\n";
		}
		return ret += "]" ;
	}
	
	private boolean estaContenido(MapMarker marcador) {
		boolean ret = false;
		double latMarcador = marcador.getLat();
		double lonMarcador = marcador.getLon();
		
		for(MapMarker marc : marcadores){
			double lat = marc.getLat();
			double lon = marc.getLon();
			ret |= lat == latMarcador && lon == lonMarcador;
		}	
		return ret;
	}
	
}
