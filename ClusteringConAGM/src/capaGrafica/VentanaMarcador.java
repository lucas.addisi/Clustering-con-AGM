package capaGrafica;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class VentanaMarcador extends JFrame {

	private static final long serialVersionUID = -3904924613773601735L;

	private Marcadores marcadores;

	private JPanel contentPane;
	private JTextField insertarLatitud;
	private JTextField insertarLongitud;
	
	
	public VentanaMarcador(Marcadores marc) {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 500);

		contentPane = new JPanel();
		marcadores = marc;

		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		jLabelInsertarLatitud();
		jTesxtFieldInsertarLatitud();

		jLabelInsertarLongitud();
		jTesxtFieldInsertarLongitud();

		botonCargar();
		botonRegresar();
	}

	private void jTesxtFieldInsertarLatitud() {
		insertarLatitud = new JTextField();
		insertarLatitud.setBounds(319, 35, 115, 20);
		contentPane.add(insertarLatitud);
		insertarLatitud.setColumns(10);
	}

	private void jTesxtFieldInsertarLongitud() {
		insertarLongitud = new JTextField();
		insertarLongitud.setBounds(319, 88, 115, 20);
		contentPane.add(insertarLongitud);
		insertarLongitud.setColumns(10);
	}

	private void jLabelInsertarLatitud() {
		JLabel insertarLatitud = new JLabel("Insertar Latitud");
		insertarLatitud.setBounds(40, 35, 115, 20);
		contentPane.add(insertarLatitud);
	}

	private void jLabelInsertarLongitud() {
		JLabel insertarLongitud = new JLabel("Insertar Longitud");
		insertarLongitud.setBounds(40, 88, 115, 20);
		contentPane.add(insertarLongitud);
	}

	private void botonCargar() { 
		JButton cargar = new JButton("Cargar a mapa");
		cargar.setBounds(319, 134, 115, 23);
		contentPane.add(cargar);
		cargar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				asignacionDeMarcadores();
				limpiarJTextField();
			}
			
			private void asignacionDeMarcadores() {
				Double lat = null;
				Double lon = null;
				try{
					lat = castear(insertarLatitud.getText());
				}
				catch(Exception n){
					JOptionPane.showMessageDialog(null, "Latitud incorrecta");
					lat = null;
				}
				
				try{
					lon = castear(insertarLongitud.getText());
				}
				catch(Exception n){
					JOptionPane.showMessageDialog(null, "Longitud incorrecta");
					lon = null;
				}
				
				if(lat != null && lon != null){
					try {
					marcadores.agregarMarcador(lat, lon);
					JOptionPane.showMessageDialog(null, "Cargado con exito");
					}
					catch (Exception e){
						JOptionPane.showMessageDialog(null, e.getMessage());
					}
				}
			}
		
		});
	}

	private void botonRegresar() {
		JButton regresar = new JButton("Regresar");
		regresar.setBounds(317, 236, 117, 25);
		contentPane.add(regresar);

		regresar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				limpiarJTextField();
				dispose();		
			}
		});
	}

	private Double castear(String latLon) throws Exception {
		return Double.parseDouble(latLon);
	}

	private void limpiarJTextField() {
		insertarLongitud.setText("");
		insertarLatitud.setText("");
	}

}
