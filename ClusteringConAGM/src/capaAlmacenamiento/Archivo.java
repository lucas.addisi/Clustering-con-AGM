package capaAlmacenamiento;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import capaGrafica.Marcadores;

import java.util.Scanner;

import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

public class Archivo implements Serializable {

	private static final long serialVersionUID = 1L;

	Gson gson;

	public Archivo() {
		gson = new Gson();
	}

	public ArrayList<Coordenada> cargarGrafo(String archivo) throws Exception {
		String jsonString = "";
		FileInputStream fis = new FileInputStream(archivo);
		Scanner scanner = new Scanner(fis);

		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			jsonString = jsonString + line;
		}

		scanner.close();

		ArrayList<Coordenada> lista = gson.fromJson(jsonString, new TypeToken<ArrayList<Coordenada>>() {
		}.getType());
		return lista;
	}

	public void guardarGrafo(Marcadores marcadores) throws Exception {

		String fileName = momentoActual();
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		ArrayList<Coordenada> coordenadas = new ArrayList<Coordenada>();

		for (MapMarker marc : marcadores.getMarcadores()) {

			Coordenada coord = new Coordenada(marc.getLat(), marc.getLon());
			coordenadas.add(coord);
		}

		String stringJson = gson.toJson(coordenadas);
		String path = "C:\\Users\\Public\\" + fileName;

		try {
			FileWriter writer = new FileWriter(path);
			writer.write(stringJson);
			writer.close();
		} catch (Exception e) {
			System.out.println("Error al guardar el archivo: " + e.getMessage());
		}

	}

	public String momentoActual() {

		Calendar calendar = Calendar.getInstance();

		Integer a�o = calendar.get(Calendar.YEAR);
		Integer mes = calendar.get(Calendar.MONTH);
		Integer dia = calendar.get(Calendar.DATE);
		Integer hor = calendar.get(Calendar.HOUR);
		Integer min = calendar.get(Calendar.MINUTE);
		Integer seg = calendar.get(Calendar.SECOND);

		return a�o.toString() + mes.toString() + dia.toString() + hor.toString() + min.toString() + seg.toString();
	}

	public static class Coordenada {
		private double latitud;
		private double longitud;

		public Coordenada(double lat, double lon) {
			latitud = lat;
			longitud = lon;
		}

		public double getLat() {
			return latitud;
		}

		public double getLon() {
			return longitud;
		}
	}

}
