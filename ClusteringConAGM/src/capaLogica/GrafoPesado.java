package capaLogica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class GrafoPesado {
	private double[][] pesos;
	private ArrayList<Set<Integer>> vecinos;
	private int aristas;

	GrafoPesado(int n) {
		aristasNegativas(n);

		pesos = new double[n][n];
		vecinos = new ArrayList<Set<Integer>>();

		for (int i = 0; i < n; i++)
			vecinos.add(new HashSet<Integer>());
	}

	public double getPesoDeArista(int i, int j) {
		aristasEnRango(i, j);
		return pesos[i][j];
	}

	public int getArista() {
		return aristas;
	}

	public void agregarArista(int i, int j, double pesoArista) {
		aristasEnRango(i, j);
		if (i == j)
			throw new IllegalArgumentException(
					"Valores " + i + " " + j + " son iguales" + "no se pueden agregar aristas a un mismo nodo.");
		if (pesoArista <= 0)
			throw new IllegalArgumentException("Valor " + pesoArista + " incorrecto");

		pesos[i][j] = pesos[j][i] = pesoArista;
		aristas++;
		vecinos.get(i).add(j);
		vecinos.get(j).add(i);
	}

	public int cantidadDeVertices() {
		return vecinos.size();
	}

	public Set<Integer> getVecinos(int i) {
		aristasEnRango(i);
		return vecinos.get(i);
	}

	public void borrarArista(int i, int j) {
		aristasEnRango(i, j);
		aristas--;
		pesos[i][j] = pesos[j][i] = 0;
		vecinos.get(i).remove(j);
		vecinos.get(j).remove(i);
	}

	public String toString() {
		String ret = "";
		for (int i = 0; i < cantidadDeVertices(); i++) {
			for (int j = 0; j < cantidadDeVertices(); j++) {
				ret += getPesoDeArista(i, j) + " ";
			}
			ret += "\n";
		}
		return ret;
	}

	public boolean equals(Object obj) {
		boolean ret = true;
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;

		ret &= getArista() == ((GrafoPesado) obj).getArista();

		for (int i = 0; i < cantidadDeVertices(); i++) {
			ret &= getVecinos(i).equals(((GrafoPesado) obj).getVecinos(i));
			for (int j = 0; j < cantidadDeVertices(); j++) {
				ret &= getPesoDeArista(i, j) == ((GrafoPesado) obj).getPesoDeArista(i, j);
			}
		}
		return ret;
	}

	private void aristasEnRango(int i, int j) {
		if (i < 0 || i >= cantidadDeVertices() || j < 0 || j >= cantidadDeVertices())
			throw new IllegalArgumentException("Valores " + i + ", " + j + " incorrectos");
	}

	private void aristasEnRango(int i) {
		if (i < 0 || i >= cantidadDeVertices())
			throw new IllegalArgumentException("Valor " + i + " incorrecto");
	}

	private void aristasNegativas(int n) {
		if (n <= 0)
			throw new IllegalArgumentException("Valor " + n + " no valido");
	}

}
