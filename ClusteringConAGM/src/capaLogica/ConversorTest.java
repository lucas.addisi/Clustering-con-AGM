package capaLogica;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import capaGrafica.Marcadores;

public class ConversorTest {

	public void getClustersEnMarcadorTest() {
		Marcadores marcadores = new Marcadores();
		marcadores.agregarMarcador(1, 2);
		marcadores.agregarMarcador(2, 2);
		marcadores.agregarMarcador(3, 2);
		
		marcadores.agregarMarcador(234.3, 2);
		marcadores.agregarMarcador(234.6, 2);
		marcadores.agregarMarcador(237.8, 2);
				
		Conversor convertir = new Conversor(marcadores);
		ArrayList<Marcadores> clusters = convertir.clustersEnMarcadores(2);
		
		ArrayList<Marcadores> resultado = new ArrayList<>();
		Marcadores r1 = new Marcadores();
		Marcadores r2 = new Marcadores();
		
		r1.agregarMarcador(1, 2);
		r1.agregarMarcador(2, 2);
		r1.agregarMarcador(3, 2);
		
		r2.agregarMarcador(234.3, 2);
		r2.agregarMarcador(234.6, 2);
		r2.agregarMarcador(237.8, 2);
		
		resultado.add(r1);
		resultado.add(r2);
		
		assertEquals(resultado, clusters);
	}

	@Test
	public void marcadorToGrafoPesadoTest() {
		Conversor conversor = new Conversor(instanciaMarcador1());
		GrafoPesado resultado = instancia1();
		GrafoPesado comparacion = conversor.marcadoresToGrafoPesado();

		Conversor conversor2 = new Conversor(instanciaMarcador2());
		GrafoPesado resultado2 = instancia2();
		GrafoPesado comparacion2 = conversor2.marcadoresToGrafoPesado();

		assertEquals(resultado, comparacion);
		assertEquals(resultado2, comparacion2);

	}

	@Test
	public void grafoToMarcadoresTest() {
		Conversor conv = new Conversor(instanciaMarcador1());
		ArrayList<Set<Integer>> conjuntos = new ArrayList<>();
		Set<Integer> conj1 = new HashSet<Integer>();
		Set<Integer> conj2 = new HashSet<Integer>();		
		conj1.add(1);
		conj1.add(3);
		conj2.add(2);
		conj2.add(0);
		conjuntos.add(conj1);
		conjuntos.add(conj2);
		
		ArrayList<Marcadores> resultado = conv.grafosToMarcadores(conjuntos);
		
		ArrayList<Marcadores> comparacion = new ArrayList<>();
		Marcadores mar1 = new Marcadores();
		Marcadores mar2 = new Marcadores();
		
		mar1.agregarMarcador(1.0, 1.0);
		mar1.agregarMarcador(2.0, 2.0);
		mar2.agregarMarcador(1.0, 0.0);
		mar2.agregarMarcador(0.0, 1.0);

		comparacion.add(mar1);
		comparacion.add(mar2);
		
		assertEquals(resultado, comparacion);
		
	}

	@Test
	public void asignarMarcadoresTest() {
		Marcadores marcadores = instanciaMarcador1();
		Conversor conv = new Conversor(marcadores);

		Set<Integer> conjunto = new HashSet<Integer>();
		conjunto.add(1);
		conjunto.add(2);
		conjunto.add(3);

		Marcadores resultado = conv.asignarMarcadores(conjunto);

		Marcadores comparacion = new Marcadores();
		comparacion.agregarMarcador(1.0, 1.0);
		comparacion.agregarMarcador(0.0, 1.0);
		comparacion.agregarMarcador(2.0, 2.0);

		assertEquals(resultado, comparacion);
	}

	@Test
	public void generarAristasTest() {
		Marcadores marcadores = instanciaMarcador1();

		Conversor conversor = new Conversor(marcadores);
		GrafoPesado grafo = new GrafoPesado(4);
		GrafoPesado comparacion = instancia1();

		conversor.generarAristas(grafo);

		assertEquals(comparacion, grafo);
	}

	private Marcadores instanciaMarcador1() {
		Marcadores marcadores = new Marcadores();
		marcadores.agregarMarcador(1.0, 0.0); // 0
		marcadores.agregarMarcador(1.0, 1.0); // 1
		marcadores.agregarMarcador(0.0, 1.0); // 2
		marcadores.agregarMarcador(2.0, 2.0); // 3
		return marcadores;
	}

	private GrafoPesado instancia1() {
		GrafoPesado grafo = new GrafoPesado(4);
		grafo.agregarArista(0, 2, Conversor.generarDistancia(1.0, 0.0, 0.0, 1.0));
		grafo.agregarArista(0, 3, Conversor.generarDistancia(1.0, 0.0, 2.0, 2.0));
		grafo.agregarArista(0, 1, Conversor.generarDistancia(1.0, 0.0, 1.0, 1.0));
		grafo.agregarArista(1, 3, Conversor.generarDistancia(1.0, 1.0, 2.0, 2.0));
		grafo.agregarArista(1, 2, Conversor.generarDistancia(1.0, 1.0, 0.0, 1.0));
		grafo.agregarArista(2, 3, Conversor.generarDistancia(0.0, 1.0, 2.0, 2.0));

		return grafo;
	}

	private Marcadores instanciaMarcador2() {
		Marcadores marcadores = new Marcadores();
		marcadores.agregarMarcador(0.0, 1.0);
		return marcadores;
	}

	private GrafoPesado instancia2() {
		GrafoPesado grafo = new GrafoPesado(1);
		return grafo;
	}

}
