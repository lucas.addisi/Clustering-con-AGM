package capaLogica;

import java.util.ArrayList;
import java.util.Set;

import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import capaGrafica.Marcadores;

public class Conversor {
	ArrayList<Tupla> contenedoras;
	
	public Conversor(Marcadores marcadores){
		contenedoras = new ArrayList<>();
		int i = 0 ;
		for (MapMarker marc : marcadores){
			contenedoras.add(new Tupla(marc,i));
			i++;
		}
	}
 	
	public ArrayList<Marcadores> clustersEnMarcadores(int cantCluster){
		ArrayList<Marcadores> ret = new ArrayList<Marcadores>();
		GrafoPesado grafo = marcadoresToGrafoPesado();
		ArrayList<Set<Integer>> clusters = Cluster.generarClusters(grafo, cantCluster);
		ret = grafosToMarcadores(clusters);
		
		return ret;
	} 
	
	GrafoPesado marcadoresToGrafoPesado(){	
		if (contenedoras.size() == 0)
			throw new IllegalArgumentException("Inserte marcadores");
		GrafoPesado grafo = new GrafoPesado(contenedoras.size());
		generarAristas(grafo);
		return grafo;
	}
	
	ArrayList<Marcadores> grafosToMarcadores(ArrayList<Set<Integer>> clusters){
		ArrayList<Marcadores> marcadores = new ArrayList<Marcadores>();
		for(Set<Integer> g: clusters){
			marcadores.add(asignarMarcadores(g));
		}
		return marcadores;
	}
	
	Marcadores asignarMarcadores(Set<Integer> g) {
		Marcadores ret = new Marcadores();
		for(Integer i : g)
			ret.agregarMarcador(contenedoras.get(i).getLat(), 
					contenedoras.get(i).getLon());
		
		return ret;
	}

	static double generarDistancia(double lat1, double lon1, double lat2, double lon2) {
		/* Obtenido de http://stackoverflow.com/questions/3694380/
		calculating-distance-between-two-points-using-latitude-longitude-what-am-i-doi */

		final int R = 6371; // radio de la tierra

		Double latDistancia = Math.toRadians(lat2 - lat1);
		Double lonDistancia = Math.toRadians(lon2 - lon1);
		Double a = Math.sin(latDistancia / 2) * Math.sin(latDistancia / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistancia / 2) * Math.sin(lonDistancia / 2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distancia = R * c * 1000; 

		distancia = Math.pow(distancia, 2);

		return Math.sqrt(distancia);
	}
	
	void generarAristas(GrafoPesado grafo) {
		for (int i = 0; i < grafo.cantidadDeVertices(); i++){
			for (int j = i; j < grafo.cantidadDeVertices(); j++) if(j != i){
				double distancia = generarDistancia(contenedoras.get(i).getLat()
						, contenedoras.get(i).getLon(), contenedoras.get(j).getLat(),
						contenedoras.get(j).getLon());
				
				grafo.agregarArista(i, j, distancia);
			}
		}
		
	}

	static class Tupla{
		private MapMarker coordenada;
		private int id;
		
		public Tupla(MapMarker c, int i){
			coordenada = new MapMarkerDot(c.getLat(), c.getLon());
			id = i;
		}
		
		public MapMarker getCoordenada(){
			return coordenada;
		}
		
		public int getID(){
			return id;
		}
		
		public double getLat(){
			return coordenada.getLat();
		}
	
		public double getLon(){
			return coordenada.getLon();
		}
		
	}

}