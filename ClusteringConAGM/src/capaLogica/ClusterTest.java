package capaLogica;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import capaLogica.Cluster.Arista;

public class ClusterTest {

	@Test
	public void generarClusrerTest() {
		GrafoPesado grafo1 = instancia1();

		ArrayList<Set<Integer>> resultado1 = new ArrayList<>();
		Set<Integer> graph1 = new HashSet<Integer>();
		Set<Integer> graph2 = new HashSet<Integer>();
		graph1.add(0);
		graph1.add(1);
		graph2.add(3);
		graph2.add(2);
		resultado1.add(graph1);
		resultado1.add(graph2);

		ArrayList<Set<Integer>> comparacion1 = Cluster.generarClusters(grafo1, 2);

		assertEquals(comparacion1, resultado1);

		ArrayList<Set<Integer>> comparacion2 =Cluster.generarClusters(grafo1, 1);

		ArrayList<Set<Integer>> resultado2 = new ArrayList<>();
		Set<Integer> graph3 = new HashSet<Integer>();
		graph3.add(0);
		graph3.add(1);
		graph3.add(2);
		graph3.add(3);
		resultado2.add(graph3);

		assertEquals(comparacion2, resultado2);

		GrafoPesado grafo2 = instancia2();

		ArrayList<Set<Integer>> resultado3 = new ArrayList<>();
		Set<Integer> graph5 = new HashSet<Integer>();
		Set<Integer> graph6 = new HashSet<Integer>();
		Set<Integer> graph7 = new HashSet<Integer>();

		graph5.add(0);
		graph6.add(2);
		graph7.add(1);
		graph7.add(3);
		graph7.add(4);
		graph7.add(5);
		graph7.add(6);
		graph7.add(7);

		resultado3.add(graph5);
		resultado3.add(graph6);
		resultado3.add(graph7);

		ArrayList<Set<Integer>> comparacion3 = Cluster.generarClusters(grafo2, 3);

		assertTrue(comparacion3.contains(resultado3.get(0)));
		assertTrue(comparacion3.contains(resultado3.get(1)));
		assertTrue(comparacion3.contains(resultado3.get(2)));
		assertFalse(comparacion3.contains(resultado2));
	}

	@Test
	public void generarClusterUnitarioTest() {
		ArrayList<Set<Integer>> clusters = new ArrayList<>();
		Set<Integer> cluster = new HashSet<>();
		cluster.add(0);
		clusters.add(cluster);

		GrafoPesado grafo = new GrafoPesado(1);

		assertEquals(clusters, Cluster.generarClusters(grafo, 1));
	}

	@Test
	public void separarClusterTest() {
		GrafoPesado agm = new GrafoPesado(4);
		agm.agregarArista(0, 1, 5);
		agm.agregarArista(2, 3, 1);

		Cluster.Arista eliminada = new Arista(1, 2, 6);

		ArrayList<Set<Integer>> comparacion = new ArrayList<>();
		for (int i = 0; i < 2; i++)
			comparacion.add(new HashSet<Integer>());

		comparacion.get(0).add(0);
		comparacion.get(0).add(1);
		comparacion.get(1).add(2);
		comparacion.get(1).add(3);

		ArrayList<Set<Integer>> visitadas = new ArrayList<>();
		for (int i = 0; i < 2; i++)
			visitadas.add(new HashSet<Integer>());

		ArrayList<Set<Integer>> totalVisitadas = new ArrayList<>();
		for (int i = 0; i < 2; i++)
			totalVisitadas.add(new HashSet<Integer>());

		Cluster.separarCluster(agm, 0, visitadas.get(0), totalVisitadas.get(0));
		Cluster.separarCluster(agm, eliminada.getFin(), visitadas.get(1), totalVisitadas.get(1));

		assertEquals(visitadas.get(0), comparacion.get(0));
		assertEquals(visitadas.get(1), comparacion.get(1));
	}

	@Test
	public void eliminarAristaTest() {
		GrafoPesado grafo = instancia1();
		Arista[] aristasEliminadas = Cluster.eliminarArista(2, grafo);

		Arista[] resultado = new Arista[1];
		Arista arista = new Arista(0, 3, 7);
		resultado[0] = arista;

		GrafoPesado grafo1 = instancia2();
		Arista[] aristasEliminadas1 = Cluster.eliminarArista(3, grafo1);

		Arista[] resultado1 = new Arista[2];
		Arista arista1 = new Arista(2, 3, 10);
		Arista arista2 = new Arista(3, 7, 10);
		resultado1[0] = arista1;
		resultado1[1] = arista2;

		assertEquals(resultado[0], aristasEliminadas[0]);
		assertEquals(resultado1[0], aristasEliminadas1[0]);
		assertEquals(resultado1[1], aristasEliminadas1[1]);

	}

	@Test
	public void generarAGMTest() {
		GrafoPesado grafo = instancia1();
		GrafoPesado comparacion = Cluster.generarAGM(grafo);

		GrafoPesado grafo2 = instancia2();
		GrafoPesado comparacion2 = Cluster.generarAGM(grafo2);

		GrafoPesado grafo3 = new GrafoPesado(2);
		grafo3.agregarArista(0, 1, 5.65);

		GrafoPesado resultado = new GrafoPesado(4);
		resultado.agregarArista(0, 1, 5);
		resultado.agregarArista(3, 2, 1);
		resultado.agregarArista(0, 2, 6);

		GrafoPesado resultado2 = new GrafoPesado(8);
		resultado2.agregarArista(0, 1, 5);
		resultado2.agregarArista(2, 1, 9);
		resultado2.agregarArista(1, 3, 4);
		resultado2.agregarArista(4, 3, 5);
		resultado2.agregarArista(4, 6, 1);
		resultado2.agregarArista(4, 5, 2);
		resultado2.agregarArista(7, 6, 4);

		GrafoPesado resultado3 = Cluster.generarAGM(grafo3);

		assertEquals(comparacion, resultado);
		assertEquals(comparacion2, resultado2);
		assertEquals(grafo3, resultado3);
		assertNotEquals(comparacion, resultado2);

	}

	@Test
	public void equalsAristaTest() {
		Cluster.Arista arista1 = new Arista(1, 2, 10);
		Cluster.Arista arista2 = new Arista(1, 2, 10);
		Cluster.Arista arista3 = new Arista(1, 2, 5);
		Cluster.Arista arista4 = null;

		assertEquals(arista1, arista2);
		assertNotEquals(arista4, arista2);
		assertNotEquals(arista3, arista2);
	}

	@Test
	public void menorEncontradaTest() {
		Cluster.Arista arista = new Arista(4, 6, 1);
		Cluster.Arista arista2 = new Arista(4, 6, -10);
		GrafoPesado grafo = instancia2();
		Set<Integer> recorridas = new HashSet<Integer>();
		recorridas.add(0);
		recorridas.add(1);
		recorridas.add(2);
		recorridas.add(3);
		recorridas.add(4);

		assertEquals(arista, Cluster.menorEcontrada(grafo, recorridas));
		assertNotEquals(arista2, Cluster.menorEcontrada(grafo, recorridas));
	}

	@Test
	public void mayorEncontradaTest() {

		GrafoPesado grafo1 = instancia1();
		Arista mayor1 = new Arista(0, 3, 7);

		GrafoPesado grafo2 = instancia2();
		Arista mayor2 = new Arista(3, 2, 10);

		GrafoPesado grafo3 = new GrafoPesado(2);
		grafo3.agregarArista(0, 1, 3.5);
		Arista mayor3 = new Arista(0, 1, 3.5);

		assertEquals(mayor1, Cluster.mayorEncontrada(grafo1));
		assertEquals(mayor2, Cluster.mayorEncontrada(grafo2));
		assertEquals(mayor3, Cluster.mayorEncontrada(grafo3));
		assertNotEquals(mayor1, Cluster.mayorEncontrada(grafo2));
	}

	private GrafoPesado instancia1() {
		GrafoPesado grafo = new GrafoPesado(4);
		grafo.agregarArista(0, 1, 5);
		grafo.agregarArista(0, 2, 6);
		grafo.agregarArista(0, 3, 7);
		grafo.agregarArista(3, 2, 1);
		return grafo;
	}

	private GrafoPesado instancia2() {
		GrafoPesado grafo = new GrafoPesado(8);
		grafo.agregarArista(0, 1, 5);
		grafo.agregarArista(1, 2, 9);
		grafo.agregarArista(1, 3, 4);
		grafo.agregarArista(2, 3, 10);
		grafo.agregarArista(3, 4, 5);
		grafo.agregarArista(3, 7, 10);
		grafo.agregarArista(4, 5, 2);
		grafo.agregarArista(4, 6, 1);
		grafo.agregarArista(5, 6, 7);
		grafo.agregarArista(6, 7, 4);

		return grafo;
	}

}
