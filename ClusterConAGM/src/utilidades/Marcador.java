package utilidades;

import java.util.ArrayList;
import java.util.Iterator;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

public class Marcador implements Iterable<MapMarker> {
	ArrayList<MapMarker> marcadores;
	
	public Marcador(){
		marcadores = new ArrayList<MapMarker>();
	}

	public int cantidadDeMarcadores() {
		return marcadores.size();
	}
	
	public ArrayList<MapMarker> getMarcadores(){
		return marcadores;
	}
	
	public void addMarcador(double lat, double lon){
		MapMarker marcador = new MapMarkerDot(lat, lon);
		marcadores.add(marcador);
	}
	
	@Override
	public Iterator<MapMarker> iterator() {
		return marcadores.iterator();
	}
	
	public boolean equals(Object obj){
		boolean ret = true;
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;
		
		ret &= marcadores.size() == ((Marcador)obj).marcadores.size();
		int i = 0;
		for(MapMarker m : marcadores){
			ret &= m.getLat() == ((Marcador)obj).marcadores.get(i).getLat()
					&& m.getLon()== ((Marcador)obj).marcadores.get(i).getLon();
			i++;
		}
		return ret;
	}
	
	public String toString(){
		String ret = "[";
		for(MapMarker c : marcadores)
		{
			ret += "Latitud: " + c.getLat() + 
					" Longitud" + c.getLon() + "\n";
		}
		return ret += "]" ;
	}
	
	public MapPolygon graficarCluster(){
		ArrayList<Coordinate> coordenadas = new ArrayList<Coordinate>();
		for(MapMarker marcador : marcadores){
			Coordinate coordenada = new Coordinate(marcador.getLat(), marcador.getLon());
			coordenadas.add(coordenada);
		}
		//TODO queda desordenado el poligono
		MapPolygon ret = new MapPolygonImpl(coordenadas);
		return ret;
	}

}
