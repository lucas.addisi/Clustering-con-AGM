package capaGrafica;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;

import javax.swing.JButton;

public class MainForm {

	private JFrame frame;
	private VentanaMapa mapa;
	private VentanaMarcador ventanaMarcador;
	private JLabel lblNewLabel;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		mapa = new VentanaMapa();
		ventanaMarcador = new VentanaMarcador();

		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		
		jLabelCabecera();
		botonMostrarMapa();
		botonVentanaMarcador();
	}

	private void jLabelCabecera() {
		lblNewLabel = new JLabel("Clusters");
		lblNewLabel.setBounds(141, 11, 173, 50);
		frame.getContentPane().add(lblNewLabel);

	}

	private void botonMostrarMapa() {
		JButton button = new JButton("Mapa");
		button.setBounds(141, 65, 142, 23);
		frame.getContentPane().add(button);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				mapa.setMarcadores(ventanaMarcador.getMarcador());
				mapa.setPoligono(ventanaMarcador.getMarcador().graficarCluster());
				ventanaMarcador.setVisible(false);
				mapa.setVisible(true);
			}

		});
	}

	private void botonVentanaMarcador() {
		JButton botonColocarMarcador = new JButton("Colocar Marcador");
		botonColocarMarcador.setBounds(141, 99, 142, 23);
		frame.getContentPane().add(botonColocarMarcador);
		botonColocarMarcador.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				ventanaMarcador.setVisible(true);
				mapa.setVisible(false);
			}

		});
	}

}
