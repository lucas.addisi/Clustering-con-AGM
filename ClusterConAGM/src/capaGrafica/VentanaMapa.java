package capaGrafica;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

import utilidades.Marcador;

import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class VentanaMapa extends JFrame {
	private static final long serialVersionUID = 3450275075573527232L;
	private JMapViewer miMapa;
	private JButton btnNewButton;
	private JToggleButton insertarNodo;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaMapa frame = new VentanaMapa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaMapa() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);

		inicializadorMapa();
		botonGenerarCluster();
		botonInsertarNodo();
		botonRegresar();
	}

	public void setMarcadores(Marcador marcadores) {
		for (MapMarker marcador : marcadores.getMarcadores())
			miMapa.addMapMarker(marcador);
	}

	public void setPoligono(MapPolygon poligono){
		miMapa.addMapPolygon(poligono);
	}
	
	private void inicializadorMapa() {
		miMapa = new JMapViewer();

		miMapa.setZoomContolsVisible(false);
		miMapa.setDisplayPositionByLatLon(-34.521, -58.7008, 14);

		setContentPane(miMapa);
	}

	private void botonRegresar() {
		JButton regresar = new JButton("Regresar");
		GroupLayout gl_miMapa = new GroupLayout(miMapa);
		gl_miMapa.setHorizontalGroup(
			gl_miMapa.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_miMapa.createSequentialGroup()
					.addGap(194)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(insertarNodo, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE))
				.addComponent(regresar, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE)
		);
		gl_miMapa.setVerticalGroup(
			gl_miMapa.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_miMapa.createSequentialGroup()
					.addComponent(regresar, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addGap(213)
					.addGroup(gl_miMapa.createParallelGroup(Alignment.BASELINE)
						.addComponent(insertarNodo)
						.addComponent(btnNewButton)))
		);
		miMapa.setLayout(gl_miMapa);

		regresar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				miMapa.setDisplayPositionByLatLon(-34.521, -58.7008, 14);
				setVisible(false);

			}

		});
	}

	private void botonGenerarCluster() {
		btnNewButton = new JButton("GenerarClusters");
	}

	private void botonInsertarNodo() {
		insertarNodo = new JToggleButton("Insertar nodo");
	}

}
