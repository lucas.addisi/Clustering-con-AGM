package capaGrafica;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import utilidades.Marcador;

import javax.swing.JTextField;
import javax.swing.JLabel;

public class VentanaMarcador extends JFrame {

	private static final long serialVersionUID = -3904924613773601735L;
	
	private Marcador marcadores;
	
	private JPanel contentPane;
	private JTextField insertarLatitud;
	private JTextField insertarLongitud;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaMarcador frame = new VentanaMarcador();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public VentanaMarcador() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		
		contentPane = new JPanel();
		marcadores = new Marcador();
		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		jLabelInsertarLatitud();
		jTesxtFieldInsertarLatitud();

		jLabelInsertarLongitud();
		jTesxtFieldInsertarLongitud();
		
		botonCargar();
		botonRegresar();
	}
	
	public Marcador getMarcador(){
		return marcadores;
	}
	
	private void jTesxtFieldInsertarLatitud() {
		insertarLatitud = new JTextField();
		insertarLatitud.setBounds(199, 35, 115, 20);
		contentPane.add(insertarLatitud);
		insertarLatitud.setColumns(10);
	}

	private void jTesxtFieldInsertarLongitud() {
		insertarLongitud = new JTextField();
		insertarLongitud.setBounds(199, 88, 115, 20);
		contentPane.add(insertarLongitud);
		insertarLongitud.setColumns(10);
	}

	private void jLabelInsertarLatitud() {
		JLabel insertarLatitud = new JLabel("Insertar Latitud");
		insertarLatitud.setBounds(40, 35, 115, 20);
		contentPane.add(insertarLatitud);
	}

	private void jLabelInsertarLongitud() {
		JLabel insertarLongitud = new JLabel("Insertar Longitud");
		insertarLongitud.setBounds(40, 88, 115, 20);
		contentPane.add(insertarLongitud);
	}

	private void botonCargar() {
		JButton cargar = new JButton("Cargar a mapa");
		cargar.setBounds(199, 138, 115, 23);
		contentPane.add(cargar);
		cargar.addActionListener(new ActionListener()  {
			@Override
			public void actionPerformed(ActionEvent e) {
				Double lat = castear(insertarLatitud.getText());
				Double lon = castear(insertarLongitud.getText());
				marcadores.addMarcador(lat, lon);
			}
		});
	}
	
	private void botonRegresar() {
		JButton regresar = new JButton("Regresar");
		// TODO El bot�n siempre debe estar en el borde inferior izquierdo.
		//Limpiar  ventana
		regresar.setBounds(317, 236, 117, 25);
		contentPane.add(regresar);
		
		regresar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
			}
		});
	}
	
	private Double castear(String latLon){
		//FIXME que pasa si no es un double?
		return Double.parseDouble(latLon);
	}

}
