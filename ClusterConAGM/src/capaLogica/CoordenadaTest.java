package capaLogica;

import static org.junit.Assert.*;

import org.junit.Test;

public class CoordenadaTest {

	@Test
	public void getTest() {
		Coordenada coor1 = new Coordenada(10.1, 11.4);
		
		assertTrue(coor1.getLat() == 10.1);
		assertTrue(coor1.getLon() == 11.4);
	}

	@Test
	public void equalsTest(){
		Coordenada coor1 = new Coordenada(10.1, 11.4);
		Coordenada coor2 = new Coordenada(10.1, 11.4);
		Coordenada coor3 = new Coordenada(11.1, 11.4);
		Coordenada coor4 = null;
		
		assertEquals(coor1, coor2);
		assertNotEquals(coor1, coor3);
		assertNotEquals(coor1, coor4);
	}
	
	@Test
	public void toStringTest(){
		Coordenada coor1 = new Coordenada(10.1, 11.4);
		
		assertEquals(coor1.toString(), "Coordenada[10.1, 11.4]");
	}

}
