package capaLogica;

public class Coordenada {
	double lat;
	double lon;

	Coordenada(double latitud, double longitud) {
		lat = latitud;
		lon = longitud;
	}

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

	public double getDistancia(Coordenada x) {
		return 0; // TODO dar distnacia
	}

	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != getClass())
			return false;

		double latitud = ((Coordenada) obj).getLat();
		double longitud = ((Coordenada) obj).getLon();
		return lat == latitud && longitud == lon;
	}

	public String toString() {
		return "Coordenada[" + lat + ", " + lon + "]";
	}

}
