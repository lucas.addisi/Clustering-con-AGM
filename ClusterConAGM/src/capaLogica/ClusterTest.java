package capaLogica;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import capaLogica.Cluster.Arista;

public class ClusterTest {

	@Test
	public void generarClustersTest() {
		Cluster cluster = new Cluster();
		
		GrafoPesado grafo1 = instancia1();
		ArrayList<GrafoPesado> comparacion1 = cluster.generarClusters(grafo1, 2);
		ArrayList<GrafoPesado> resultado1 = new ArrayList<>();
		resultado1.get(0).setArista(0, 1, 5);
		resultado1.get(1).setArista(3, 2, 1);

		assertEquals(resultado1.get(0), comparacion1.get(0));
		assertEquals(resultado1.get(1), comparacion1.get(1));
		
		GrafoPesado grafo2 = instancia2();
		ArrayList<GrafoPesado> comparacion2 = cluster.generarClusters(grafo2, 2);
		ArrayList<GrafoPesado> resultado2 = new ArrayList<GrafoPesado>();
		resultado2.get(0).setArista(0, 1, 5);
		resultado2.get(0).setArista(1, 3, 4);
		resultado2.get(0).setArista(4, 3, 5);
		resultado2.get(0).setArista(4, 6, 1);
		resultado2.get(0).setArista(4, 5, 2);
		resultado2.get(0).setArista(7, 6, 4);
		
		assertEquals(comparacion2.get(0), resultado2.get(0));
		assertEquals(comparacion2.get(1), resultado2.get(1));
		
		//GrafoPesado comparacion3 = cluster.generarClusters(grafo2, 3);
		GrafoPesado resultado3 = new GrafoPesado(8);
		resultado3.setArista(1, 3, 4);
		resultado3.setArista(4, 3, 5);
		resultado3.setArista(4, 6, 1);
		resultado3.setArista(4, 5, 2);
		resultado3.setArista(7, 6, 4);
		
		assertEquals(comparacion3, resultado3);
		
		ArrayList<GrafoPesado> comparacion4 = cluster.generarClusters(grafo2, 8);
		ArrayList<GrafoPesado> resultado4 = new ArrayList<GrafoPesado>();
		
		assertEquals(comparacion4, resultado4);
	}

	@Test
	public void generarAGMTest() {
		GrafoPesado grafo = instancia1();
		Cluster cluster = new Cluster();
		GrafoPesado comparacion = cluster.generarAGM(grafo);

		GrafoPesado grafo2 = instancia2();
		Cluster cluster2 = new Cluster();
		GrafoPesado comparacion2 = cluster2.generarAGM(grafo2);

		GrafoPesado grafo3 = new GrafoPesado(2);
		grafo3.setArista(0, 1, 5.65);

		GrafoPesado resultado = new GrafoPesado(4);
		resultado.setArista(0, 1, 5);
		resultado.setArista(3, 2, 1);
		resultado.setArista(0, 2, 6);

		GrafoPesado resultado2 = new GrafoPesado(8);
		resultado2.setArista(0, 1, 5);
		resultado2.setArista(2, 1, 9);
		resultado2.setArista(1, 3, 4);
		resultado2.setArista(4, 3, 5);
		resultado2.setArista(4, 6, 1);
		resultado2.setArista(4, 5, 2);
		resultado2.setArista(7, 6, 4);

		GrafoPesado resultado3 = cluster.generarAGM(grafo3);

		assertEquals(comparacion, resultado);
		assertEquals(comparacion2, resultado2);
		assertEquals(grafo3, resultado3);
		assertNotEquals(comparacion, resultado2);

	}

	@Test
	public void equalsAristaTest() {
		Cluster.Arista arista1 = new Arista(1, 2, 10);
		Cluster.Arista arista2 = new Arista(1, 2, 10);
		Cluster.Arista arista3 = new Arista(1, 2, 5);
		Cluster.Arista arista4 = null;

		assertEquals(arista1, arista2);
		assertNotEquals(arista4, arista2);
		assertNotEquals(arista3, arista2);
	}

	@Test
	public void menorEncontradaTest() {
		Cluster cluster = new Cluster();
		Cluster.Arista arista = new Arista(4, 6, 1);
		Cluster.Arista arista2 = new Arista(4, 6, -10);
		GrafoPesado grafo = instancia2();
		Set<Integer> recorridas = new HashSet<Integer>();
		recorridas.add(0);
		recorridas.add(1);
		recorridas.add(2);
		recorridas.add(3);
		recorridas.add(4);

		assertEquals(arista, cluster.menorEcontrada(grafo, recorridas));
		assertNotEquals(arista2, cluster.menorEcontrada(grafo, recorridas));
	}

	@Test
	public void mayorEncontradaTest(){
		Cluster cluster = new Cluster();
		
		GrafoPesado grafo1 = instancia1();
		Arista mayor1 = new Arista(0, 3, 7);
		
		GrafoPesado grafo2 = instancia2();
		Arista mayor2 = new Arista(3,2,10);
		
		GrafoPesado grafo3 = new GrafoPesado(2);
		grafo3.setArista(0, 1, 3.5);
		Arista mayor3 = new Arista(0, 1, 3.5);
	
		assertEquals(mayor1,cluster.mayorEncontrada(grafo1));
		assertEquals(mayor2,cluster.mayorEncontrada(grafo2));
		assertEquals(mayor3,cluster.mayorEncontrada(grafo3));
		assertNotEquals(mayor1,cluster.mayorEncontrada(grafo2));
	}
	
	private GrafoPesado instancia1() {
		GrafoPesado grafo = new GrafoPesado(4);
		grafo.setArista(0, 1, 5);
		grafo.setArista(0, 2, 6);
		grafo.setArista(0, 3, 7);
		grafo.setArista(3, 2, 1);
		return grafo;
	}

	private GrafoPesado instancia2() {
		GrafoPesado grafo = new GrafoPesado(8);
		grafo.setArista(0, 1, 5);
		grafo.setArista(1, 2, 9);
		grafo.setArista(1, 3, 4);
		grafo.setArista(2, 3, 10);
		grafo.setArista(3, 4, 5);
		grafo.setArista(3, 7, 10);
		grafo.setArista(4, 5, 2);
		grafo.setArista(4, 6, 1);
		grafo.setArista(5, 6, 7);
		grafo.setArista(6, 7, 4);

		return grafo;
	}

}
