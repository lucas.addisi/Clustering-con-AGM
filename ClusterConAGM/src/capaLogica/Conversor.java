package capaLogica;

import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import utilidades.Marcador;

public class Conversor {
	ArrayList<Tupla> contenedoras;
	
	public Conversor(Marcador marcadores){
		contenedoras = new ArrayList<>();
		int i = 0 ;
		for (MapMarker marc : marcadores){
			contenedoras.add(new Tupla(marc,i));
			i++;
		}
	}
 	
	public ArrayList<Marcador> getClustersEnMarcador(int cantCluster){
		ArrayList<Marcador> ret = new ArrayList<Marcador>();
		GrafoPesado grafo = marcadorToGrafoPesado();
		Cluster generadorDeClusters = new Cluster();
		ArrayList<GrafoPesado> clusters = generadorDeClusters.generarClusters(grafo, cantCluster);
		ret = grafosToMarcadores(clusters);
		
		return ret;
	}
	
	GrafoPesado marcadorToGrafoPesado(){	
		GrafoPesado grafo = new GrafoPesado(contenedoras.size());
		generarAristas(grafo);
		return grafo;
	}
	
	ArrayList<Marcador> grafosToMarcadores(ArrayList<GrafoPesado> clusters){
		ArrayList<Marcador> marcadores = new ArrayList<Marcador>();
		for(GrafoPesado g: clusters){
			marcadores.add(asignarMarcadores(g));
		}
		return marcadores;
	}
	
	Marcador asignarMarcadores(GrafoPesado g) {
		Marcador ret = new Marcador();
		for(int i = 0; i < g.cantidadDeVertices(); i++)
			ret.addMarcador(contenedoras.get(i).getLat(), 
					contenedoras.get(i).getLon());
		
		return ret;
	}

	void generarAristas(GrafoPesado grafo) {
		for (int i = 0; i < grafo.cantidadDeVertices(); i++){
			for (int j = i; j < grafo.cantidadDeVertices(); j++) if(j != i){
				grafo.setArista(i, j, i+1);//TODO	generarDistacias
			}
		}
		
	}

	static class Tupla{
		private MapMarker coordenada;
		private int id;
		
		public Tupla(MapMarker c, int i){
			coordenada = new MapMarkerDot(c.getLat(), c.getLon());
			id = i;
		}
		
		public int getID(){
			return id;
		}
		
		public double getLat(){
			return coordenada.getLat();
		}
	
		public double getLon(){
			return coordenada.getLon();
		}
		
	}

}