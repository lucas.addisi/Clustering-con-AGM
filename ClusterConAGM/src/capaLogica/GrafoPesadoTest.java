package capaLogica;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class GrafoPesadoTest {

	@Test
	public void setAristaTest() {
		GrafoPesado grafo = instancia1();
		ArrayList<Set<Integer>> vecinos = instanciaVecinos1();
		vecinos.get(0).add(1);
		vecinos.get(0).add(2);
		vecinos.get(0).add(3);
		vecinos.get(1).add(0);
		vecinos.get(2).add(0);
		vecinos.get(2).add(3);
		vecinos.get(3).add(0);
		vecinos.get(3).add(2);

		// Iguales aristas
		assertTrue(grafo.getPesoDeArista(0, 1) == 5);
		assertTrue(grafo.getPesoDeArista(0, 2) == 6);
		assertTrue(grafo.getPesoDeArista(0, 3) == 7);
		assertTrue(grafo.getPesoDeArista(3, 2) == 1);
		// Al reves tambien
		assertTrue(grafo.getPesoDeArista(1, 0) == 5);
		assertTrue(grafo.getPesoDeArista(2, 0) == 6);
		assertTrue(grafo.getPesoDeArista(3, 0) == 7);
		assertTrue(grafo.getPesoDeArista(2, 3) == 1);

		assertEquals(grafo.getVecinos(0), vecinos.get(0));
		assertEquals(grafo.getVecinos(1), vecinos.get(1));
		assertEquals(grafo.getVecinos(2), vecinos.get(2));
		assertEquals(grafo.getVecinos(3), vecinos.get(3));

	}

	@Test(expected = IllegalArgumentException.class)
	public void setAristaErroneaTest() {
		GrafoPesado grafo = instancia1();
		grafo.setArista(2, 1, -5);
	}

	@Test(expected = IllegalArgumentException.class)
	public void setAristaIgualNodoTest() {
		GrafoPesado grafo = instancia1();
		grafo.setArista(1, 1, 5);
	}

	@Test
	public void getAristasTest() {
		GrafoPesado grafo = instancia1();
		assertTrue(grafo.getArista() == 4);
		assertFalse(grafo.getArista() == 1);
	}

	@Test
	public void getVecinosTest() {
		GrafoPesado grafo = instancia1();
		HashSet<Integer> set0 = new HashSet<Integer>();
		HashSet<Integer> set1 = new HashSet<Integer>();
		HashSet<Integer> set2 = new HashSet<Integer>();
		HashSet<Integer> set3 = new HashSet<Integer>();

		set0.add(1);
		set0.add(2);
		set0.add(3);
		set1.add(0);
		set2.add(0);
		set2.add(3);
		set3.add(0);
		set3.add(2);

		assertEquals(grafo.getVecinos(0), set0);
		assertEquals(grafo.getVecinos(1), set1);
		assertEquals(grafo.getVecinos(2), set2);
		assertEquals(grafo.getVecinos(3), set3);

	}

	@Test
	public void getPesoDeAristaTest() {
		GrafoPesado grafo = instancia1();

		assertTrue(grafo.getPesoDeArista(0, 1) == 5);
		assertTrue(grafo.getPesoDeArista(0, 2) == 6);
		assertTrue(grafo.getPesoDeArista(0, 3) == 7);
		assertTrue(grafo.getPesoDeArista(2, 3) == 1);
		assertTrue(grafo.getPesoDeArista(1, 0) == 5);
		assertTrue(grafo.getPesoDeArista(2, 0) == 6);
		assertTrue(grafo.getPesoDeArista(3, 0) == 7);
		assertTrue(grafo.getPesoDeArista(3, 2) == 1);

	}

	@Test
	public void toStringTest() {
		GrafoPesado grafo = instancia1();
		String comparativo = "0.0 5.0 6.0 7.0 \n" + "5.0 0.0 0.0 0.0 \n" + "6.0 0.0 0.0 1.0 \n" + "7.0 0.0 1.0 0.0 \n";

		assertEquals(grafo.toString(), comparativo);
	}

	@Test
	public void equalsTest(){
		GrafoPesado grafo = instancia1();
		GrafoPesado comparacion = new GrafoPesado(4);
		GrafoPesado comparacion2 = new GrafoPesado(4);
		GrafoPesado nulo = null;
		Integer otroObjeto = new Integer(2);
		
		comparacion.setArista(3, 0, 7);
		comparacion.setArista(2, 0, 6);
		comparacion.setArista(1, 0, 5);
		comparacion.setArista(2, 3,1);
		
		comparacion2.setArista(3, 0, 7);
		comparacion2.setArista(2, 0, 6);
		comparacion2.setArista(1, 0, 5);
		comparacion2.setArista(2, 3, 2);
		
		assertEquals(grafo, comparacion);
		assertNotEquals(grafo, comparacion2);
		assertNotEquals(grafo, nulo);
		assertNotEquals(otroObjeto, grafo);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void nodosEnRangosTest() {
		GrafoPesado grafo = instancia1();
		grafo.setArista(5, 1, 10);
		grafo.getVecinos(1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void nodosEnRangosSobrecargaTest() {
		GrafoPesado grafo = instancia1();
		grafo.getVecinos(4);
	}

	@Test(expected = IllegalArgumentException.class)
	public void nodosNegativosTest() {
		GrafoPesado grafo = new GrafoPesado(0);
		grafo.getArista();
	}

	@Test
	public void borrarAristaTest() {
		GrafoPesado grafo = instancia1();
		ArrayList<Set<Integer>> vecinos = instanciaVecinos1();
		vecinos.get(0).add(2);
		vecinos.get(0).add(3);
		vecinos.get(2).add(0);
		vecinos.get(3).add(0);

		grafo.borrarArista(1, 0);
		grafo.borrarArista(2, 3);
		assertTrue(grafo.getArista() == 2);
		assertTrue(grafo.getPesoDeArista(1, 0) == 0);
		assertTrue(grafo.getPesoDeArista(0, 1) == 0);
		assertTrue(grafo.getPesoDeArista(2, 3) == 0);
		assertTrue(grafo.getPesoDeArista(3, 2) == 0);

		assertEquals(grafo.getVecinos(0), vecinos.get(0));
		assertEquals(grafo.getVecinos(1), vecinos.get(1));
		assertEquals(grafo.getVecinos(2), vecinos.get(2));
		assertEquals(grafo.getVecinos(3), vecinos.get(3));

	}

	private ArrayList<Set<Integer>> instanciaVecinos1() {
		ArrayList<Set<Integer>> vecinos = new ArrayList<Set<Integer>>();
		for (int i = 0; i < 4; i++) {
			HashSet<Integer> vecino = new HashSet<Integer>();
			vecinos.add(vecino);
		}
		return vecinos;
	}

	private GrafoPesado instancia1() {
		GrafoPesado grafo = new GrafoPesado(4);
		grafo.setArista(0, 1, 5);
		grafo.setArista(0, 2, 6);
		grafo.setArista(0, 3, 7);
		grafo.setArista(3, 2, 1);
		return grafo;
	}

}
