package capaLogica;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Cluster {
	// TODO agregar estadísticas como atributo
	public ArrayList <GrafoPesado> generarClusters(GrafoPesado grafo, int cantCluster) {
		if (cantCluster > grafo.cantidadDeVertices()) 
			throw new IllegalArgumentException("El valor " + cantCluster
					+ " es invalido. La cantidad de Clusters debe ser mayor"
					+ " a su numero de vertices");
		
		ArrayList<GrafoPesado> grafoPartido = new ArrayList<GrafoPesado>();
		GrafoPesado grafoNoConexo = generarAGM(grafo);
		Arista[] aristasEliminadas = new Arista[cantCluster - 1];
		
		for (int i = 0; i < cantCluster - 1; i++) {
			Arista eliminar = mayorEncontrada(grafoNoConexo);
			aristasEliminadas[i] = eliminar;
			grafoNoConexo.borrarArista(eliminar.verticeInicio, eliminar.verticeFin);
		}
		
		grafoPartido = partirGrafo(grafoNoConexo, aristasEliminadas);
		return grafoPartido;
	}

	GrafoPesado generarAGM(GrafoPesado grafo) {
		GrafoPesado ret = new GrafoPesado(grafo.cantidadDeVertices());
		Set<Integer> verticesRecorridos = new HashSet<Integer>();
		verticesRecorridos.add(0);

		for (int i = 0; i < grafo.cantidadDeVertices() - 1; i++) {
			Arista temporal = menorEcontrada(grafo, verticesRecorridos);
			ret.setArista(temporal.verticeInicio, temporal.verticeFin, temporal.pesoArista);
			verticesRecorridos.add(temporal.verticeFin);
		}
		return ret;
	}

	Arista menorEcontrada(GrafoPesado grafo, Set<Integer> verticesRecorridos) {
		Arista ret = new Arista(-10, -10, Double.MAX_VALUE);

		for (Integer i : verticesRecorridos)
			for (Integer j : grafo.getVecinos(i))
				if (!verticesRecorridos.contains(j)) {
					if (grafo.getPesoDeArista(i, j) < ret.pesoArista)
						ret = new Arista(i, j, grafo.getPesoDeArista(i, j));
				}

		return ret;
	}

	Arista mayorEncontrada(GrafoPesado grafo) {
		Arista mayorEncontrada = new Arista(0, 0, Double.MIN_VALUE);
		for (int i = 0; i < grafo.cantidadDeVertices() - 1; i++) {
			for (Integer j : grafo.getVecinos(i)) {
				Arista temporal = new Arista(i, j, grafo.getPesoDeArista(i, j));
				if (temporal.getPesoArista() > mayorEncontrada.getPesoArista())
					mayorEncontrada = temporal;
			}
		}
		return mayorEncontrada;
	}

	private ArrayList<GrafoPesado> partirGrafo(GrafoPesado grafoNoConexo, Arista[] eliminadas) {
		ArrayList <GrafoPesado> clusters = new ArrayList<>();
		return null;
	}
	
	static class Arista {
		int verticeInicio;
		int verticeFin;
		double pesoArista;

		public Arista(int inicio, int fin, double peso) {
			verticeInicio = inicio;
			verticeFin = fin;
			pesoArista = peso;
		}

		public int getInicio() {
			return verticeInicio;
		}

		public int getFin() {
			return verticeFin;
		}

		public double getPesoArista() {
			return pesoArista;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;

			if (obj == null || getClass() != obj.getClass())
				return false;

			Arista arista = (Arista) obj;
			return ((verticeInicio == arista.verticeInicio && verticeFin == arista.verticeFin)
					|| (verticeFin == arista.verticeInicio && verticeInicio == arista.verticeFin))
					&& arista.pesoArista == pesoArista;
		}

	}

}
