package capaLogica;

import static org.junit.Assert.*;

import org.junit.Test;

import utilidades.Marcador;

public class ConversorTest {

	public void  getClustersEnMarcadorTest(){
		Marcador marcadores = instanciaMarcador1();
		Conversor convertir = new Conversor(marcadores);
		convertir.getClustersEnMarcador(2);
		
		//TODO saber distancias
	}
	
	@Test
	public void marcadorToGrafoPesadoTest() {
		Conversor conversor = new Conversor(instanciaMarcador1());
		GrafoPesado resultado = instancia1();
		GrafoPesado comparacion = conversor.marcadorToGrafoPesado();

		Conversor conversor2 = new Conversor(instanciaMarcador2());
		GrafoPesado resultado2 = instancia2();
		GrafoPesado comparacion2 = conversor2.marcadorToGrafoPesado();

		assertEquals(resultado, comparacion);
		assertEquals(resultado2, comparacion2);

	}
	
	@Test
	public void grafoToMarcadorTest(){
		GrafoPesado grafo = instancia1();
		Conversor conversor = new Conversor(instanciaMarcador1());
		Marcador resultado = conversor.asignarMarcadores(grafo);
		
		assertEquals(resultado, instanciaMarcador1());
	}
	
	@Test
	public void asignarMarcadoresTest() {
	}

	@Test
	public void generarAristasTest() {
		Marcador marcadores = instanciaMarcador1();

		Conversor conversor = new Conversor(marcadores);
		GrafoPesado grafo = new GrafoPesado(4);
		GrafoPesado comparacion = instancia1();

		conversor.generarAristas(grafo);

		assertEquals(comparacion, grafo);
	}
	
	private Marcador instanciaMarcador1() {
		Marcador marcadores = new Marcador();
		marcadores.addMarcador(1.0, 0.0);
		marcadores.addMarcador(1.0, 1.0);
		marcadores.addMarcador(0.0, 1.0);
		marcadores.addMarcador(2.0, 2.0);
		return marcadores;
	}

	private GrafoPesado instancia1() {
		GrafoPesado comparacion = new GrafoPesado(4);
		comparacion.setArista(0, 2, 1);
		comparacion.setArista(0, 3, 1);
		comparacion.setArista(0, 1, 1);
		comparacion.setArista(1, 3, 2);
		comparacion.setArista(1, 2, 2);
		comparacion.setArista(2, 3, 3);

		return comparacion;
	}

	private Marcador instanciaMarcador2() {
		Marcador marcadores = new Marcador();
		marcadores.addMarcador(0.0, 1.0);
		return marcadores;
	}

	private GrafoPesado instancia2() {
		GrafoPesado grafo = new GrafoPesado(1);
		return grafo;
	}

}
